import processing.pdf.*;

import java.awt.Robot;
import java.awt.Rectangle;
import java.awt.AWTException;

PImage screenshot;
String timestamp;

UI ui;

void setup() {
  colorMode(HSB, 360, 100, 100);
  
  ui = new UI(this);
  fullScreen(P2D);
  //size(500, 500, P2D);
  
  smooth(4);
}

void draw() {
  setTimeStamp();
  ui.background();
  ui.evalButtons();
}

/*
 * Prevention from making an accidental screenshot. JIC.
 */
//void keyPressed() {
//  if (key == 's' || key == 'S') {
//    screenshot();
//  }
//}

void setTimeStamp() {
  timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-"  + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
}

void screenshot() {
  try {
    Robot robot = new Robot();
    screenshot = new PImage(robot.createScreenCapture(new Rectangle(0, 0, width, height)));
    screenshot.save(savePath("IMG-" + timestamp + "####.png"));
  } catch (AWTException e) {
    println("We got an AWTexception");
    exit();
  }
}
