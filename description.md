## Wood

The aim of this work was to generate wooden texture.
It widely uses perlin noise for background genteration
and distance map algorithm for line genteration on the top of the background.
User can display up to five different types of wood.

In order to run the application, please, download [Processing](https://processing.org/download/).
Then open [Wood.pde](./Wood.pde) file and click play button in upper left corner.

Have fun :)