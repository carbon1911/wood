/*
 * Draws lines on the background.
 */
void drawLines(PImage distanceIm, int[][] yCenters, PVector theColor) {

  colorMode(HSB, 360, 100, 100);

  int LINE_COUNT = 0;
  int LINE_SPACE = 0;
  float NOISE_SEED = 0;

  LINE_COUNT = height / 17;
  LINE_SPACE = height / LINE_COUNT;  

  float yCoord = 0;
  int distance = 0;

  for (float y = -LINE_SPACE; y < height; y += LINE_SPACE) {
    strokeWeight(random(height / 500, 2.5 * height / 500));
    stroke(theColor.x, theColor.y, theColor.z + random(8), 75);
    for (float x = 0; x < width; ++x) {
      yCoord = y + 100 * noise(NOISE_SEED);

      distance = distanceIm.get((int)x, (int)yCoord) - 0xFF000000;
      
      /*
       * If point p from line l hits point p1 from distance map (@see #distanceIm)
       * p has collided with an ellipse, l has to get around.
       * If p's y coordinate > value of pixel p1, p is below the center of
       * colliding ellipse e and l will continue below e.
       * l will continue above e otherwise.
       */
      if (distance != 0 && withinIm((int)x, (int)yCoord)) {
        if (yCoord > yCenters[(int)yCoord][(int)x]) {
          yCoord = moveOnBorder((int)x, (int)yCoord, true, distanceIm);
        } else {
          yCoord = moveOnBorder((int)x, (int)yCoord, false, distanceIm);
        }
      }
      point(x, yCoord);
      NOISE_SEED += random(0.002, 0.004);
    }
  }
}

/*
 * Returns true if coordinate x and y are within image range
 */
private boolean withinIm(int x, int y) {
  return x >= 0 && x < width && y >= 0 && y < height;
}

/*
 * Since values from distance map (@see #distanceIm) are not optimal for a line to get
 * around an ellipse, we need this little method for improvement. This method makes
 * a line fit an edge of an ellipse.
 */
private int moveOnBorder(int x, int y, boolean direction, PImage distanceIm) {
  int distance = distanceIm.get(x, y) - 0xFF000000;
  for (int _ = 0; _ < 3; ++_) {
    y += (direction ? distance : -distance);
    distance = distanceIm.get(x, y) - 0xFF000000;
  }
  return y;
}
