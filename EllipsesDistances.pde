class EllipsesDistances {
  
  /* Buffer for ellipses in final image. */
  PGraphics rings;
  
  /* Buffer for approximate position of rings. Used for subsequent calculation of distance map */
  PImage auxEllipses;
  
  /* Distance map buffer. */
  PImage dist;
  
  /* Buffer used to store y coordinate of a ellipse's center */
  int[][] yCenters = new int[height][width];
  
  /* This list stores information about centers of all rings, their widths and heights */
  ArrayList<float[]> ringsData = new ArrayList<float[]>();
  
  EllipsesDistances() {
    auxEllipses = auxEllipses();
    distanceMap();
  }  
  
// --------------- P U B L I C  I N T E R F A C E -------------------------------------
  
  PImage getauxEllipses() {
    return auxEllipses;
  }
  
  PImage getDistances() {
    return dist;
  }
  
  int[][] getYCenters() {
    return yCenters;
  }
  
  /*
   * Draws rings using color @param theColor.
   */
  void drawAllRings(PVector theColor) {
    rings = createGraphics(width, height);
    rings.beginDraw();
    rings.colorMode(HSB, 360, 100, 100);
    rings.fill(theColor.x, theColor.y, theColor.z);
    rings.stroke(theColor.x, theColor.y, theColor.z + 3);
    for (float[] e : ringsData) {
      drawRings(e[0], e[1], e[2], e[3]);
    }
    PImage nonBlurred = rings.copy();
    rings.filter(BLUR, 2);
    rings.endDraw();
    image(rings, 0, 0);
    image(nonBlurred, 0, 0);
  }
  
// ----------------- P R I V A T E -----------------------------------------------------
  
    
  /*
   * Fills out buffer for subsequent calculation of distance map and
   * @see #yCenters buffer.
   */
  private PImage auxEllipses() {
    noStroke();
    background(0);
    fill(255, 0, 0);
    
    final int size = height * width;
    final double squaresDensity = 10. / size;
    final double from = size / 10e4;
    final double to = from * 5;
    
    float radX = 0.;
    float radY = 0.;
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        if (random(1) < squaresDensity && yCenters[y][x] == 0) {
          radX = random((int)from, (int)to);
          radY = random(0.66 * radX, radX);
          if (radX < width / 100) {
            continue;
          }
          ellipse(x, y, 2 * radX, radY);
          
          ringsData.add(new float[]{x, y, radX, radY});
          
          markYCoord(x, y, (int)radX, (int)radY);
          x += radX;
          y += radY;
        }
      }
    }
    
    return copy();
  }
  
  /*
   * Calculates distance map using two-pass algorithm from PB130 course.
   */
  private void distanceMap() {
    dist = createImage(width, height, RGB); //<>//
    
    setInfinity(dist);
    
    for (int y = 1; y < height - 1; y++) {
      for (int x = 1; x < width - 1; x++) {
        dist.set(x, y, auxEllipses.get(x, y) == #000000 ? #000000 : color(Math.min(minFwd(dist, x, y), Integer.MAX_VALUE)));
      }
    }
    
    for (int y = height - 2; y > 0; y--) {
      for (int x = width - 2; x > 0; x--) {
        dist.set(x, y, color(Math.min(dist.get(x, y), minBwd(dist, x, y))));
      }
    }
  }
  
  
  /*
   * Sets all pixels to infinite.
   */
  private void setInfinity(PImage pimg) {
    int size = pimg.width * pimg.height;
    pimg.loadPixels();
    for (int i = 0; i < size; ++i) {
      pimg.pixels[i] = Integer.MAX_VALUE;
    }
    pimg.updatePixels();
  }
  
  /*
   * Chooses pixel with minimal value from a region. See below to see which pixels affect this method.
   * 
   * y y y
   * y p n
   * n n n,
   *
   * where p is current pixel with coordinates @param x and @param y,
   * y are pixels from 8-neighborhood of p affected by this method and
   * n are pixels from 8-neighborhood of p ignored by this method.
   *
   * @see #distanceMap() method which uses this method in first pass of scan.
   */
  private int minFwd(PImage sp, int x, int y) {
    return Math.min(Math.min(Math.min(sp.get(x - 1, y) + 1,
                      sp.get(x - 1, y - 1) + 1),
                      sp.get(x, y - 1) + 1),
                      sp.get(x + 1, y - 1) + 1);
  }

  /*
   * Chooses pixel with minimal value from a region. See below to see which pixels affect this method.
   * 
   * n n n
   * n p y
   * y y y,
   *
   * where p is current pixel with coordinates @param x and @param y,
   * y are pixels from 8-neighborhood of p affected by this method and
   * n are pixels from 8-neighborhood of p ignored by this method.
   *
   * @see #distanceMap() method which uses this method in second pass of scan.
   */
  private int minBwd(PImage sp, int x, int y) {
    return Math.min(Math.min(Math.min(sp.get(x - 1, y + 1) + 1,
                      sp.get(x, y + 1) + 1),
                      sp.get(x + 1, y + 1) + 1),
                      sp.get(x, y + 1) + 1);
  }
  
  /*
   * When drawing an ellipse e in @see #auxEllipses image, it is necessary to
   * mark y coordinate of center of e in each coordinate which belongs to e
   * in @see #auxEllipses image. This methods marks y coordinate in rectangle
   * bounding box of e to @see #yCenters array.
   *
   * Marked array @see #yCenters is then used by @see #Line#drawLines(...) method
   * to decide if the line should be drawn above the ellipse or below.
   */
  private void markYCoord(int xCenter, int yCenter, int radX, int radY) {
    for (int y = yCenter - radY; y < yCenter + radY; ++y) {
      for (int x = xCenter - radX; x < xCenter + radX; ++x) {
        if (withinIm(x, y)) {
          yCenters[y][x] = yCenter;
        }
      }
    }
  }
  
  /*
   * Returns true if coordinate x and y are within image range.
   */
  private boolean withinIm(int x, int y) {
    return x >= 0 && x < width && y >= 0 && y < height; 
  }
  
  /*
   * Draws six concentric ellipses with @param xCenter, @param yCenter center and
   * @param radX major width and @param radY minor width.
   */
  private void drawRings(float xCenter, float yCenter, float radX, float radY) {
    rings.strokeWeight(radX / 50);
    rings.alpha((int)random(100));
    for (int i = 6; i > 0; --i) {
      rings.ellipse(xCenter, yCenter, i * radX/6, i * radY/6);
    }
  }
}
