/*
 * This file contains colors using which wood is drawn.
 * Color is represented in HSB format.
 * Variables starting with letter l(ight) are used for background, the ones starting
 * with letter d(ark) are used to draw curves and circles on the top of the backgroud
 */

final PVector lWillow = new PVector(51, 34, 48);
final PVector dWillow = new PVector(41, 40, 37);

final PVector lOak = new PVector(38, 52, 38);
final PVector dOak = new PVector(37, 50, 25);

final PVector lSpruce = new PVector(33, 59, 30);
final PVector dSpruce = new PVector(32, 51, 18);

final PVector lDarkOak = new PVector(29, 72, 17);
final PVector dDarkOak = new PVector(30, 72, 10);

final PVector lRWood = new PVector(26, 45, 41);
final PVector dRWood = new PVector(24, 52, 24);
