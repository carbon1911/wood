import controlP5.*;

class UI { 
  
  final int borderH = height / 16;
  final int buttonOff = width / 20;
  final int buttonH = height / 50;
  final int buttonW = width / 5 - buttonOff;
  final int buttonX = buttonOff / 2;
  final int buttonY = (int)Math.ceil(0.6 * borderH);
  final int fontSize = (int)Math.ceil(1.1 * buttonH);
  PFont font = createFont("Rockwell", fontSize);
  
  ControlP5 cp5;
  Button exit, willow, oak, spruce, dOakButton, rWood;
  
  Background texture;
  
  UI(Wood w) {
    texture = new Background();
    cp5 = new ControlP5(w);
    
    exit = cp5.addButton("X")
               .setPosition(width - width / 18, buttonH - height / 60)
               .setSize(width / 20, buttonH)
               .setFont(font)
               .setColorBackground(#802D00)
               .setColorForeground(#A43200)
               .setColorActive(#FF3200);
               
    willow = cp5.addButton("willow")
               .setPosition(buttonX, buttonY)
               .setSize(buttonW, buttonH)
               .setFont(font);
               
    oak = cp5.addButton("oak")
               .setPosition(buttonX + (buttonW + buttonOff), buttonY)
               .setSize(buttonW, buttonH)
               .setFont(font);
               
    spruce = cp5.addButton("spruce")
               .setPosition(buttonX + 2*(buttonW + buttonOff), buttonY)
               .setSize(buttonW, buttonH)
               .setFont(font);
               
    dOakButton = cp5.addButton("dark oak")
               .setPosition(buttonX + 3*(buttonW + buttonOff), buttonY)
               .setSize(buttonW, buttonH)
               .setFont(font);
               
    rWood = cp5.addButton("rosewood")
               .setPosition(buttonX + 4*(buttonW + buttonOff), buttonY)
               .setSize(buttonW, buttonH)
               .setFont(font);
  }
  
  // --------------- P U B L I C  I N T E R F A C E -------------------------------------
  
  void evalButtons() {
    if (exit.isPressed()) {     
      exit();
    }
    if (willow.isPressed()) {
      draw(lWillow, dWillow);
    }
    if (oak.isPressed()) {
      draw(lOak, dOak);
    }
    if (spruce.isPressed()) {
      draw(lSpruce, dSpruce);
    }
    if (dOakButton.isPressed()) {
      draw(lDarkOak, dDarkOak);
    }
    if (rWood.isPressed()) {
      draw(lRWood, dRWood);
    }
  }
  
  /*
   * Draws grey panel under buttons.
   */
  void background() {
    rectMode(CORNER);
    noStroke();
    colorMode(RGB, 255, 255, 255);
    fill(120);
    rect(0, 0, width, borderH);
  }
  
  //----------------- P R I V A T E -----------------------------------------------------
  
  /*
   * Actual drawing of background and foreground lines & ellipses.
   */
  private void draw(PVector lightColor, PVector darkColor) {
    texture.fill(lightColor);
    EllipsesDistances ed = new EllipsesDistances();
    image(ed.getDistances(), 0, 0);
    
    image(texture.getBackground(), 0, 0);
    drawLines(ed.getDistances(), ed.getYCenters(), darkColor);
    
    ed.drawAllRings(darkColor);
  } 
}
