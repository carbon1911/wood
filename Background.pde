class Background {
  PGraphics bckg;
  
  Background() {
    bckg = createGraphics(width, height, P2D);
  }
  
//--------------- P U B L I C  I N T E R F A C E -----------------------------------------------

  /*
   * Creates background texture fill.
   * Use image() function to draw texture after calling this function.
   */
  public void fill(PVector theColor) {
    bckg.beginDraw();
    
    bckg.noStroke();
    bckg.colorMode(HSB, 360, 100, 100);
    
    final int RECT_SIDE = 10;
    final float NOISE_STEP = random(10, 20) / 1000.;
    final int COLS = width / RECT_SIDE;
    final int ROWS = height / RECT_SIDE;
    
    float noise_value = 0;
    for (int y = 0; y <= ROWS; y++) {
      for (int x = 0; x <= COLS; x++) {
        noise_value = noise(x * NOISE_STEP, y * NOISE_STEP);
  
        bckg.fill(theColor.x + (int)random(-2, 2), theColor.y + (int)random(-2, 2), map(noise_value, 0, 1, 0, theColor.z + 50));
        
        bckg.rect(x * RECT_SIDE, y * RECT_SIDE, RECT_SIDE, RECT_SIDE);
      }
    }
    //bckg.filter(DILATE);
    bckg.filter(BLUR, 2);
    bckg.endDraw();
  }

  public PGraphics getBackground() {
    return bckg;
  }
}
